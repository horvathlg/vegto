# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	$('.datatable').DataTable({
		"language": {
			"lengthMenu": "_MENU_ cikk oldalanként",
			"zeroRecords": "Nincs találat.",
			"info": "_PAGE_. oldal a _PAGES_-ból/ből.",
			"infoFiltered": "(találat az _MAX_ cikkből.)"
		}
	})