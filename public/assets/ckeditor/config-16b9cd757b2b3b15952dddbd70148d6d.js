(function() {
  CKEDITOR.editorConfig = function(config) {
    config.language = "hu";
    config.font_defaultLabel = 'Arial';
    config.fontSize_defaultLabel = '16px';
    return true;
  };

}).call(this);
